<?php
error_reporting(0);
include_once 'connection.php';
include_once 'user-data.php';

$database = new Database();
$db = $database->getConnect();

$fetch = new Users($db);

// Read users data
if ($_SERVER['REQUEST_METHOD'] === "GET") {
    if (isset($_GET['str']) and $_GET['str'] == "all") {
        $ud   =   $fetch->getUserData($_GET['str']);
        echo $ud;
    }

    if (isset($_GET['str'])) {
        list($flag, $ids)  =   explode(":", $_GET['str']);
        //get user profile
        if ($flag == "profile") {
            $ud         =   $fetch->getUserData($ids);
            $userAry    =   json_decode($ud);
            echo '<div class="container"><h2>User Details</h2><table class="table-user"><tr><td width="80">User ID</td><td>' . $userAry[0]->userid . '</td></tr><tr><td>Name</td><td>' . $userAry[0]->username . '</td></tr><tr><td>Email</td><td>' . $userAry[0]->email . '</td></tr><tr><td>Status</td><td>' . $userAry[0]->status . '</td></tr><tr><td>Datetime</td><td>' . $userAry[0]->created . '</td></tr></tbody></table></div>';
        }
    }
}


// Delete users data
if ($_SERVER['REQUEST_METHOD'] === "DELETE") {
    if (isset($_GET['str'])) {
        list($flag, $ids)  =   explode(":", $_GET['str']);
        if ($flag == "delluser") {
            $fetch->delete("tb_users", 'userid IN (' . $ids . ')');
            $ud   =   $fetch->getUserData('all');
            echo $ud;
        }
    }
}


// Add users data
if ($_SERVER['REQUEST_METHOD'] === "POST") {
    if (isset($_REQUEST['str']) and $_REQUEST['str'] == "searchdata") {
        $ud   =   $fetch->getUserData($_POST['s']);
        echo $ud;
    }

    //add user
    if (isset($_GET['str']) and $_GET['str'] == "adduser") {
        $data = array(
            'username' => $_POST['username'],
            'email' => $_POST['email'],
            'status' => $_POST['status']
        );
        $fetch->addUserData("tb_users", $data);


        $ud   =   $fetch->getUserData('all');
        echo $ud;
    }
}
