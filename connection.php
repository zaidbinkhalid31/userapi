<?php
class Database
{

	// specify your own database credentials
	private $host = "localhost";
	private $db_name = "test";
	private $username = "root";
	private $password = "";
	public $conn;

	// get the database connection
	public function getConnect()
	{
		$this->conn = null;
		try {
			$this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
			$this->conn->exec("set names utf8");
		} catch (PDOException $exception) {
			// set response code - 502 Bad Gateway
			http_response_code(502);

			echo json_encode(
				array("message" => "Connection error: " . $exception->getMessage())
			);
		}

		return $this->conn;
	}
}
