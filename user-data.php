<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

class Users
{
    // database connection and table name
    private $table_name = "tb_users";

    // object properties
    public $userid;
    public $username;
    public $email;
    public $status;
    public $created;
    public $condition;
    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getUserData($cond)
    {
        $this->condition  .=  "";
        if ($cond == "all") {
            $this->condition  .=  "";
        }
        if ($cond != "all") {
            $this->condition  .=  ' AND ((username LIKE "%' . $cond . '%") OR (email LIKE "%' . $cond . '%") OR (status LIKE "%' . $cond . '%") OR (userid="' . $cond . '") )';
        }
        // select all query
        $query = "SELECT * FROM " . $this->table_name . ' WHERE 1 ' . $this->condition;
        // prepare query statement
        $stmt = $this->conn->prepare($query);

        // execute query
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (count($rows) > 0)
            return json_encode($rows);
        else {
            // set response code - 404 Not found
            http_response_code(404);
            echo json_encode(
                array("message" => "No users found.")
            );
        }
    }

    public function addUserData($tableName, array $data)
    {
        $stmt = $this->conn->prepare("INSERT INTO $tableName (" . implode(',', array_keys($data)) . ") VALUES (" . implode(',', array_fill(0, count($data), '?')) . ")");
        try {
            $stmt->execute(array_values($data));

            return $stmt->rowCount();
        } catch (PDOException $e) {
            throw new RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }


    /**
     * Delete Method
     *
     * @param  string $tableName
     * @param  array  $where     (associative where key is field name)
     * @return int    number of affected rows
     */
    public function delete($tableName, $where)
    {
        $stmt = $this->conn->prepare("DELETE FROM $tableName WHERE " . $where);
        try {
            $stmt->execute();

            return $stmt->rowCount();
        } catch (PDOException $e) {
            throw new RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }


    /**
     * Update Method
     *
     * @param  string $tableName
     * @param  array  $set       (associative where key is field name)
     * @param  array  $where     (associative where key is field name)
     * @return int    number of affected rows
     */
    public function update(
        $tableName,
        array $set,
        array $where
    ) {
        $arrSet = array_map(
            function ($value) {
                return $value . '=:' . $value;
            },
            array_keys($set)
        );

        $stmt = $this->conn->prepare(
            "UPDATE $tableName SET " . implode(',', $arrSet) . ' WHERE ' . key($where) . '=:' . key($where) . 'Field'
        );

        foreach ($set as $field => $value) {
            $stmt->bindValue(':' . $field, $value);
        }
        $stmt->bindValue(':' . key($where) . 'Field', current($where));
        try {
            $stmt->execute();

            return $stmt->rowCount();
        } catch (PDOException $e) {
            throw new RuntimeException("[" . $e->getCode() . "] : " . $e->getMessage());
        }
    }
}
